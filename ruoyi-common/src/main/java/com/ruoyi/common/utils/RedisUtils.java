package com.ruoyi.common.utils;

import com.ruoyi.common.utils.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.params.SetParams;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Redis 工具类
 *
 * @author liyu
 */
public class RedisUtils
{
    private static Logger log = LoggerFactory.getLogger(RedisUtils.class);

    /**
     * 默认过期时长，单位：秒
     **/
    public static final long DEFAULT_EXPIRE = 60 * 60 * 24;
    /**
     * 不设置过期时长
     **/
    public static final long NOT_EXPIRE = -1;

    /**
     * 表示 SET IF NOT EXIST
     **/
    private static final String NX = "NX";
    /**
     * 表示 SET WITH EXPIRE_TIME
     **/
    private static final String EX = "EX";
    /**
     * 加锁成功
     **/
    private static final String LOCK_OK = "OK";
    /**
     * 解锁成功
     **/
    private static final Long UNLOCK_OK = 1L;
    /**
     * 解锁的脚本
     **/
    private static final String UNLOCK_SCRIPT = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
    /**
     * 请求标识
     **/
    private static ThreadLocal<String> LOCK_VALUE = new ThreadLocal<String>()
    {
        @Override
        protected String initialValue()
        {
            return UUID.randomUUID().toString();
        }
    };

    private static RedisTemplate redisTemplate = SpringUtils.getBean("redisTemplate");
    ;

    /**
     * @param key
     * @param expireSeconds 过期时间
     * @return
     * @MethodName lock
     * @Description 加锁
     */
    public static boolean lock(String key, int expireSeconds)
    {
        return (boolean) redisTemplate.execute(new RedisCallback<Boolean>()
        {
            @Override
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException
            {
                Object nativeConnection = connection.getNativeConnection();
                if (nativeConnection instanceof JedisCluster)
                {
                    JedisCluster jedisCluster = (JedisCluster) nativeConnection;

                    String result = jedisCluster.set(key, LOCK_VALUE.get(), SetParams.setParams().nx().ex(expireSeconds));
                    return LOCK_OK.equals(result);
                }
                if (nativeConnection instanceof Jedis)
                {
                    Jedis jedis = (Jedis) nativeConnection;
                    String result = jedis.set(key, LOCK_VALUE.get(), SetParams.setParams().nx().ex(expireSeconds));
                    return LOCK_OK.equals(result);
                }
                return false;
            }
        });
    }

    /**
     * @param key
     * @MethodName unlock
     * @Description 解锁
     * @Return boolean
     */
    public static boolean unlock(String key)
    {
        return (boolean) redisTemplate.execute(new RedisCallback<Boolean>()
        {
            @Override
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException
            {
                Object nativeConnection = connection.getNativeConnection();
                if (nativeConnection instanceof JedisCluster)
                {
                    JedisCluster jedisCluster = (JedisCluster) nativeConnection;
                    Object unlock = jedisCluster.eval(UNLOCK_SCRIPT, Collections.singletonList(key), Collections.singletonList(LOCK_VALUE.get()));
                    return UNLOCK_OK.equals(unlock);
                }
                if (nativeConnection instanceof Jedis)
                {
                    Jedis jedis = (Jedis) nativeConnection;
                    Object unlock = jedis.eval(UNLOCK_SCRIPT, Collections.singletonList(key), Collections.singletonList(LOCK_VALUE.get()));
                    return UNLOCK_OK.equals(unlock);
                }
                return false;
            }
        });
    }

    /**
     * 删除key
     *
     * @param key
     */
    public static void delete(String key)
    {
        redisTemplate.delete(key);
    }

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public static boolean expire(String key, long time)
    {
        try
        {
            if (time > 0)
            {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 根据key获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public static long getExpire(String key)
    {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public static boolean hasKey(String key)
    {
        try
        {
            return redisTemplate.hasKey(key);
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    public static void del(String... key)
    {
        if (key != null && key.length > 0)
        {
            if (key.length == 1)
            {
                redisTemplate.delete(key[0]);
            }
            else
            {
                redisTemplate.delete((Collection<String>) CollectionUtils.arrayToList(key));
            }
        }
    }

    /**
     * 获取缓存
     *
     * @param key 键
     * @return 值
     */
    public static Object get(String key)
    {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 添加缓存
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public static boolean set(String key, Object value)
    {
        try
        {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 添加缓存并设置过期时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public static boolean set(String key, Object value, long time)
    {
        try
        {
            if (time > 0)
            {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            }
            else
            {
                set(key, value);
            }
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key 键
     * @return
     */
    public static long incr(String key, long delta)
    {
        if (delta < 0)
        {
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key 键
     * @return
     */
    public static long decr(String key, long delta)
    {
        if (delta < 0)
        {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * 设置一组Map的键值对
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public static Object hGet(String key, String item)
    {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public static Map<Object, Object> hmGet(String key)
    {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 添加一个Map类型值
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmSet(String key, Map<String, Object> map)
    {
        try
        {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 添加一个Map类型值并设置过期时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public static boolean hmSet(String key, Map<String, Object> map, long time)
    {
        try
        {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0)
            {
                expire(key, time);
            }
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public static boolean hSet(String key, String item, Object value)
    {
        try
        {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public static boolean hSet(String key, String item, Object value, long time)
    {
        try
        {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0)
            {
                expire(key, time);
            }
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public static void hDel(String key, Object... item)
    {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public static boolean hHasKey(String key, String item)
    {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public static double hIncr(String key, String item, double by)
    {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public static double hDecr(String key, String item, double by)
    {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public static Set<Object> sGet(String key)
    {
        try
        {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e)
        {
            log.error("", e);
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public static boolean sHasKey(String key, Object value)
    {
        try
        {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public static long sSet(String key, Object... values)
    {
        try
        {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e)
        {
            log.error("", e);
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public static long sSetAndTime(String key, long time, Object... values)
    {
        try
        {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0)
            {
                expire(key, time);
            }
            return count;
        } catch (Exception e)
        {
            log.error("", e);
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public static long sGetSetSize(String key)
    {
        try
        {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e)
        {
            log.error("", e);
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public static long setRemove(String key, Object... values)
    {
        try
        {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e)
        {
            log.error("", e);
            return 0;
        }
    }

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0 到 -1代表所有值
     * @return
     */
    public static List<Object> lGet(String key, long start, long end)
    {
        try
        {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e)
        {
            log.error("", e);
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public static long lGetListSize(String key)
    {
        try
        {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e)
        {
            log.error("", e);
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public static Object lGetIndex(String key, long index)
    {
        try
        {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e)
        {
            log.error("", e);
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public static boolean lSet(String key, Object value)
    {
        try
        {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public static boolean lSet(String key, Object value, long time)
    {
        try
        {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0)
            {
                expire(key, time);
            }
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public static boolean lSet(String key, List<Object> value)
    {
        try
        {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public static boolean lSet(String key, List<Object> value, long time)
    {
        try
        {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0)
            {
                expire(key, time);
            }
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public static boolean lUpdateIndex(String key, long index, Object value)
    {
        try
        {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e)
        {
            log.error("", e);
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public static long lRemove(String key, long count, Object value)
    {
        try
        {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e)
        {
            log.error("", e);
            return 0;
        }
    }
}