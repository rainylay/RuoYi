package com.ruoyi.framework.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.handler.FastJson2JsonRedisSerializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import redis.clients.jedis.JedisPoolConfig;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * redis配置
 *
 * @author ruoyi
 */
@Configuration
public class RedisConfig extends CachingConfigurerSupport
{
    @Value("${jedis.pool.config.maxTotal:100}")
    private int jedisMaxTotal;

    @Value("${jedis.pool.config.maxWaitMillis:5000}")
    private int jedisMaxWaitMillis;

    @Value("${jedis.pool.config.maxIdle:10}")
    private int jedisMaxIdle;

    @Bean
    @Override
    public KeyGenerator keyGenerator()
    {
        return (target, method, params) -> {
            StringBuilder sb = new StringBuilder();
            sb.append(target.getClass().getName());
            sb.append(method.getName());
            for (Object obj : params)
            {
                sb.append(obj.toString());
            }
            return sb.toString();
        };
    }


    public JedisPoolConfig jedisPoolConfig()
    {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(jedisMaxTotal);
        config.setMaxIdle(jedisMaxIdle);
        config.setMaxWaitMillis(jedisMaxWaitMillis);
        return config;
    }

    @Bean(name = "redisConnectionFactory")
    public JedisConnectionFactory createJedisConnectionFactory(RedisProperties properties)
    {
        JedisPoolConfig poolConfig = jedisPoolConfig();
        JedisConnectionFactory factory;
        RedisSentinelConfiguration sentinelConfig = getSentinelConfiguration(properties);
        RedisClusterConfiguration clusterConfiguration = getClusterConfiguration(properties);
        if (sentinelConfig != null)
        {
            factory = new JedisConnectionFactory(sentinelConfig, poolConfig);
        }
        else if (clusterConfiguration != null)
        {
            factory = new JedisConnectionFactory(clusterConfiguration, poolConfig);
        }
        else
        {
            factory = new JedisConnectionFactory(poolConfig);
            factory.setHostName(properties.getHost());
            factory.setPort(properties.getPort());
            factory.setDatabase(properties.getDatabase());
        }

        if (StringUtils.hasText(properties.getPassword()))
        {
            factory.setPassword(properties.getPassword());
        }
        return factory;
    }

  /*  @Bean
    public RedisCacheManager cacheManager(RedisConnectionFactory connectionFactory) {
        return RedisCacheManager.create(connectionFactory);
    }*/

    @Bean
    public CacheManager cacheManager(@Qualifier("redisConnectionFactory") RedisConnectionFactory redisConnectionFactory)
    {
        RedisCacheConfiguration redisCacheConfiguration = instanceConfig(1L); //设置过期时间为1天
        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);
        return RedisCacheManager
                .builder(redisCacheWriter)
                .cacheDefaults(redisCacheConfiguration).transactionAware().build();
    }

    //    @Cacheable 要设置过期时间,需要下面两个方法,并且在相关调用方法上写诸如下面
    //    @Cacheable(value = "activityList",key = "'pageNum_'+#pageNum+'_pageSize_'+#pageSize",cacheManager = "redisCacheManager")
    /*@Bean("redisCacheManager")
    public RedisCacheManager redisCacheManager(RedisConnectionFactory connectionFactory)
    {
        RedisCacheConfiguration redisCacheConfiguration = instanceConfig(24L); //设置过期时间为1天
        return RedisCacheManager.builder(connectionFactory)
                .cacheDefaults(redisCacheConfiguration)
                .transactionAware()
                .build();
    }*/


    @Bean
    @SuppressWarnings(value = {"unchecked", "rawtypes"})
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory)
    {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        FastJson2JsonRedisSerializer serializer = new FastJson2JsonRedisSerializer(Object.class);
        serializer.setObjectMapper(initObjectMapper());

        template.setConnectionFactory(connectionFactory);

        // 使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());

        template.setValueSerializer(serializer);
        template.setHashValueSerializer(serializer);

        template.afterPropertiesSet();
        return template;
    }

    private ObjectMapper initObjectMapper()
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        //objectMapper.registerModule(new JavaTimeModule());
        // 去掉各种@JsonSerialize注解的解析
        //objectMapper.configure(MapperFeature.USE_ANNOTATIONS, false);
        // 只针对非空的值进行序列化
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        // 将类型序列化到属性json字符串中
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.setDateFormat(new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS));
        return objectMapper;
    }

    private RedisCacheConfiguration instanceConfig(Long ttl)
    {
        //Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);
        FastJson2JsonRedisSerializer serializer = new FastJson2JsonRedisSerializer(Object.class);
        serializer.setObjectMapper(initObjectMapper());
        return RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofHours(ttl))
                .disableCachingNullValues()
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(serializer));

    }

    private static List<RedisNode> createSentinels(RedisProperties.Sentinel sentinel)
    {
        List<RedisNode> nodes = new ArrayList<>();
        List<String> nodeList = sentinel.getNodes();
        //StringUtils.commaDelimitedListToStringArray(sentinel.getNodes())
        for (String node : nodeList)
        {
            String[] parts = StringUtils.split(node, ":");
            Assert.state(parts.length == 2, "Redis哨兵地址配置不合法！" + node);
            nodes.add(new RedisNode(parts[0], Integer.valueOf(parts[1])));
        }
        return nodes;
    }

    private static RedisSentinelConfiguration getSentinelConfiguration(RedisProperties properties)
    {
        RedisProperties.Sentinel sentinel = properties.getSentinel();
        if (sentinel != null)
        {
            RedisSentinelConfiguration config = new RedisSentinelConfiguration();
            config.master(sentinel.getMaster());
            config.setSentinels(createSentinels(sentinel));
            return config;
        }
        return null;
    }


    private static RedisClusterConfiguration getClusterConfiguration(RedisProperties properties)
    {
        RedisProperties.Cluster cluster = properties.getCluster();
        if (cluster != null)
        {
            RedisClusterConfiguration config = new RedisClusterConfiguration(cluster.getNodes());
            if (cluster.getMaxRedirects() != null)
            {
                config.setMaxRedirects(cluster.getMaxRedirects());
            }
            return config;
        }
        return null;
    }
}