package com.ruoyi.framework.interceptor;

import com.baomidou.mybatisplus.core.plugins.InterceptorIgnoreHelper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;

public class MybatisSqlMonitorInnerInterceptor implements InnerInterceptor {
	private static final Logger log = LoggerFactory.getLogger(MybatisSqlMonitorInnerInterceptor.class);

	@Override
	public void beforePrepare(StatementHandler sh, Connection connection, Integer transactionTimeout) {
		PluginUtils.MPStatementHandler mpStatementHandler = PluginUtils.mpStatementHandler(sh);
		MappedStatement mappedStatement = mpStatementHandler.mappedStatement();
		SqlCommandType sct = mappedStatement.getSqlCommandType();
		MetaObject metaObject = MetaObject
				.forObject(mpStatementHandler, SystemMetaObject.DEFAULT_OBJECT_FACTORY, SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY,
						new DefaultReflectorFactory());

		if (!InterceptorIgnoreHelper.willIgnoreIllegalSql(mappedStatement.getId())) {
			String id = mappedStatement.getId();
			log.info("-------------------- Sql Id : [{}], 类型 : [{}]", id, mappedStatement.getSqlCommandType().toString());

			BoundSql boundSql = mpStatementHandler.boundSql();
			//log.info("准备SQL: {}", boundSql.getSql());

			// 获取节点的配置
			Configuration configuration = mappedStatement.getConfiguration();
			// 获取到最终的sql语句
			String newsql = getSql(configuration, boundSql, id);
			log.info("-------------------- 执行SQL : {}", newsql);
		}
	}

	/**
	 * 封装了一下sql语句，
	 * 使得结果返回完整xml路径下的sql语句节点id + sql语句
	 *
	 * @param configuration
	 * @param boundSql
	 * @param sqlId
	 * @return
	 */
	private String getSql(Configuration configuration, BoundSql boundSql, String sqlId) {
		String sql = showSql(configuration, boundSql);
		StringBuilder str = new StringBuilder(100);
		//str.append(sqlId);
		//str.append(":");
		str.append(sql);
		return str.toString();
	}

	/**
	 * 如果参数是String，则添加单引号， 如果是日期，则转换为时间格式器并加单引号；
	 * 对参数是null和不是null的情况作了处理<br>
	 *
	 * @param obj
	 * @return
	 */
	private String getParameterValue(Object obj) {
		String value = null;
		if (obj instanceof String) {
			value = "'" + obj.toString() + "'";
		} else if (obj instanceof Date || obj instanceof LocalDateTime) {
			DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
			value = "'" + formatter.format(new Date()) + "'";
		} else {
			value = obj != null ? obj.toString() : "";
		}
		return value;
	}

	/**
	 * 进行？的替换
	 *
	 * @param configuration
	 * @param boundSql
	 * @return
	 */
	public String showSql(Configuration configuration, BoundSql boundSql) {
		// 获取参数
		Object parameterObject = boundSql.getParameterObject();
		List<ParameterMapping> parameterMappings = boundSql
				.getParameterMappings();
		// sql语句中多个空格都用一个空格代替
		String sql = beautifySql(boundSql.getSql());
		if (CollectionUtils.isNotEmpty(parameterMappings) && parameterObject != null) {
			// 获取类型处理器注册器，类型处理器的功能是进行java类型和数据库类型的转换　　　　　　　
			// 如果根据parameterObject.getClass(）可以找到对应的类型，则替换
			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
			if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
				sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(parameterObject)));
			} else {
				//MetaObject主要是封装了originalObject对象，
				// 提供了get和set的方法用于获取和设置originalObject的属性值,
				// 主要支持对JavaBean、Collection、Map三种类型对象的操作
				MetaObject metaObject = configuration.newMetaObject(parameterObject);
				for (ParameterMapping parameterMapping : parameterMappings) {
					String propertyName = parameterMapping.getProperty();
					if (metaObject.hasGetter(propertyName)) {
						Object obj = metaObject.getValue(propertyName);
						sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(obj)));
					} else if (boundSql.hasAdditionalParameter(propertyName)) {
						// 该分支是动态sql
						Object obj = boundSql.getAdditionalParameter(propertyName);
						sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(obj)));
					} else {
						//打印出缺失，提醒该参数缺失并防止错位
						sql = sql.replaceFirst("\\?", "缺失");
					}
				}
			}
		}
		return sql;
	}

	/**
	 * 美化sql
	 *
	 * @param sql sql语句
	 */
	private String beautifySql(String sql) {
		sql = sql.replace("\n", "").replace("\t", "").replaceAll("[\\s]+", " ");
		//sql = sql.replaceAll("[\\s\n]+", " ");
		return sql;
	}

}