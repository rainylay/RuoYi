package com.ruoyi.framework.shiro.util;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.util.Destroyable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <p> 自定义cacheManage 扩张shiro里面的缓存 使用reids作缓存 </p>
 * <description>
 * 引入自己定义的CacheManager
 * 关于CacheManager的配置文件在spring-redis-cache.xml中
 * </description>
 */
//@Component("shiroCacheManager")
public class ShiroSpringCacheManager implements CacheManager, Destroyable
{
    /**
     * 将之上的RedisCacheManager的Bean拿出来 注入于此
     */
    //@Autowired
    private org.springframework.cache.CacheManager cacheManager;

    public org.springframework.cache.CacheManager getCacheManager()
    {
        return cacheManager;
    }

    public void setCacheManager(org.springframework.cache.CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    @Override
    public void destroy() throws Exception
    {
        cacheManager = null;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name)
    {
        if (name == null)
        {
            return null;
        }
        // 新建一个ShiroSpringCache 将Bean放入并实例化
        return new ShiroSpringCache<K, V>(name, getCacheManager());
    }

}