package com.ruoyi.framework.handler;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.ruoyi.common.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class JacksonDateTimeParse extends JsonDeserializer<Date>
{
    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {
        SimpleDateFormat format = new SimpleDateFormat(DateUtils.YYYY_MM_DD_HH_MM_SS);
        String date = jsonParser.getText();
        if (date == null || date.trim().length() == 0)
        {
            return null;
        }
        try
        {
            return format.parse(date);
        } catch (ParseException e)
        {
            log.error("日期解析出错", e);
        }
        return null;
    }
}